//Task 1: 
#include <iostream>
#include <time.h>

int rollDie(){
	int roll;
	int min = 1; // the min number a die can roll is 1
	int max = 6;// this->dieSize; // the max value is the die size

	roll = rand() % (max - min + 1) + min;

	return roll;
}

int main(){
	srand(time(0));
	for (int i = 0; i<10; i++){
		std::cout << rollDie() << " " ;
	}
	std::cout << "\n" ;
	getchar();
}

//Task 2: 
#include<iostream>
#include<string>
#include<ctime>
#include<stdio.h>
#include<stdlib.h>
#include<algorithm>
#include<vector>

using namespace std;

bool movemin(vector <int> &v) { //Sorting a vector using bubble sort 
	for (int i = 0; i < v.size() - 2; i++) {
		for (int j = i + 1; j < v.size() - 1; j++) {
			if (v[i] > v[j]) {
				int temp = v[j];
				v[j] = v[i];
				v[i] = temp;

			}
		}
	}
	return true;
}

bool testmovemin(vector <int> &v, vector <int> &vtest) {
	clock_t start;
	start = clock();
	movemin(v);

	cout << "Running time of the movemin function " << double(clock() - start / CLOCKS_PER_SEC) << " " << endl;
	cout << "Vector after sorting " << endl;
	for (int i = 0; i < 30; i++) {
		cout << v[i] << " ";
	}
	cout << endl;
	start = clock();
	sort(vtest.begin(), vtest.end());
	cout << "Running time of the sort fnction " << double(clock() - start / CLOCKS_PER_SEC) << " " << endl;
	return 0;
}

int main(){
	//int random[30];
	clock_t start;
	vector<int> v;
	vector<int> vtest;
	srand((unsigned)time(NULL));
	int randomNumber;
	for (int i = 0; i < 30; i++){
		randomNumber = 1 + rand() % 100;
		//cout << random[i] << " ";
		v.push_back(randomNumber);
		vtest.push_back(randomNumber);
		cout << v[i] << " ";
	}
	//cout << v ;

	cout << endl;
	cout << endl;

	testmovemin(v, vtest);
	getchar();
	return 0;
}

//Task 3:
#include <iostream>
#include <vector>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <ctime>
#include <algorithm>

using namespace std;

//bool moveMin(vector<int> &in);
//bool testMoveMin(vector<int> &in, vector<int> &out);

bool moveMin(vector<int> &in) {
	bool valuesSwitched = false;
	int len = in.size();

	for (int i = 1; i <= len; i++){
		if (i == len){
			if (!valuesSwitched) { break; }
			valuesSwitched = false;
			i = 1;
		}
		if (in[i - 1] > in[i]){
			int temp = in[i - 1];
			in[i - 1] = in[i];
			in[i] = temp;
			valuesSwitched = true;
		}
	}
	return true;
}

bool testMoveMin(vector<int> &in, vector<int> &out) {
	
	clock_t start;
	start = clock();
	moveMin(in);
	cout << "Run-time of move-Min is " << double(clock() - start / CLOCKS_PER_SEC) << endl;
	cout << endl;
	start = clock();
	sort(out.begin(), out.end());
	cout << "Run-time of sort is " << double(clock() - start / CLOCKS_PER_SEC) << endl;
	cout << endl;
	return true;
}

int main(){
	vector<int> in;
	vector<int> out;
	srand(time(0));
	for (int i = 0; i < 30; i++) {
		int random_number = rand() % 100 + 1;
		in.push_back(random_number);
		out.push_back(random_number);
	}
	cout << "Unsorted array is " << endl;
	for (int i = 0; i < 30; i++) {
		cout << in[i] << " ";
	}
	cout << endl;
	cout << endl;

	testMoveMin(in, out);

	for (int i = 0; i < 30; i++) {
		cout << in[i] << " ";
	}
	cout << "\n" << endl;

	getchar();
	return 0;
}
